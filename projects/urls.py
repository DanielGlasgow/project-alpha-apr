from django.urls import path
from projects.views import (
    home,
    show_project,
    create_project,
)


urlpatterns = [
    path("", home, name="list_projects"),
    path("", home, name="root"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
