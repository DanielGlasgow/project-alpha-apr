from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required()
def home(request):
    project = Project.objects.filter(owner=request.user)
    context = {
        "home": project,
    }
    return render(request, "projects/list.html", context)


@login_required()
def show_project(request, id):
    detail = get_object_or_404(Project, id=id)
    context = {
        "detail": detail,
    }
    return render(request, "projects/detail.html", context)


@login_required()
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("root")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
